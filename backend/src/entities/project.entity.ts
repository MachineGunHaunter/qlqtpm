import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { ApiModelProperty } from '@nestjs/swagger';
import { ProjectUser } from './project-user.entity';
import { User } from './user.entity';
import { Task } from './task.entity';
import { ProjectTask } from './project-task.entity';
import { TimeEntries } from './time-entries.entity';

@Entity({ name: 'projects' })
export class Project {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 500 })
  @ApiModelProperty()
  name: string;

  @Column({
    name: 'created-at',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP'
  })
  createAt: Date;

  @Column({
    name: 'updated-at',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP'
  })
  updateAt: Date;

  @OneToMany(type => ProjectUser, projectUser => projectUser.project)
  usersProject: Promise<ProjectUser[]>;

  @OneToMany(type => ProjectTask, projectTask => projectTask.task)
  tasksProject: Promise<ProjectTask[]>;

  @OneToMany(type => TimeEntries, timeEntries => timeEntries.project)
  timeEntries: Promise<TimeEntries[]>;
}
