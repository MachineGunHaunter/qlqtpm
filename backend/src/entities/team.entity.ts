import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { ApiModelProperty } from '@nestjs/swagger';
import { User } from './user.entity';

@Entity({ name: 'teams' })
export class Team {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 500 })
  @ApiModelProperty()
  title: string;

  @Column({ length: 500, nullable: true })
  @ApiModelProperty()
  description: string;

  @Column({
    name: 'created-at',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP'
  })
  createAt: Date;

  @Column({
    name: 'updated-at',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP'
  })
  updateAt: Date;

  @Column({
    default: true
  })
  active: boolean;

  @OneToMany(type => User, user => user.team)
  users: Promise<User[]>;
}
