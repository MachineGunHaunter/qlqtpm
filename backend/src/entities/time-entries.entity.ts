import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn
} from 'typeorm';
import { Project } from './project.entity';
import { Task } from './task.entity';
import { User } from './user.entity';

@Entity({ name: 'time-entries' })
export class TimeEntries {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP'
  })
  start: Date;
  @Column({ nullable: true })
  stop: Date;
  @Column({ nullable: true })
  lastStart: Date;

  @Column({
    name: 'create-at',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP'
  })
  createAt: Date;

  @Column()
  projectId: number;

  @Column()
  taskId: number;

  @Column()
  userId: number;

  @Column({
    name: 'duration',
    nullable: true
  })
  duration: number;

  @Column({
    name: 'bill',
    nullable: true
  })
  bill: number;
  @Column({
    default: true
  })
  active: boolean;

  @Column({
    default: false
  })
  isStop: boolean;

  @ManyToOne(type => Task, task => task.timeEntries)
  task: Task;
  @ManyToOne(type => Project, project => project.timeEntries)
  project: Project;

  @ManyToOne(type => User)
  @JoinColumn({ name: 'userId' })
  user: User;
}
