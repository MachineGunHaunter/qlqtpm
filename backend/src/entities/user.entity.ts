import { ApiModelProperty } from '@nestjs/swagger';
import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn
} from 'typeorm';
import { ProjectUser } from './project-user.entity';
import { Team } from './team.entity';

@Entity({ name: 'users' })
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'full-name' })
  @ApiModelProperty()
  fullName: string;

  @Column()
  @ApiModelProperty()
  email: string;

  @Column({ name: 'user-name' })
  @ApiModelProperty()
  userName: string;

  @Column({ nullable: true })
  @ApiModelProperty()
  avatar: string;

  @Column()
  @ApiModelProperty()
  password: string;

  @Column({
    default: true
  })
  active: boolean;

  @OneToMany(type => ProjectUser, projectUser => projectUser.user)
  projectsUser: Promise<ProjectUser[]>;

  @ManyToOne(type => Team, team => team.users)
  team: Team;
}
