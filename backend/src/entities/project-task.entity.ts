import { Entity, ManyToOne, Column, PrimaryGeneratedColumn } from 'typeorm';
import { Project } from './project.entity';
import { Task } from './task.entity';

@Entity({ name: 'project-task' })
export class ProjectTask {
  @PrimaryGeneratedColumn()
  id: number;
  @ManyToOne(type => Task, task => task.projectsTask)
  task: Task;
  @ManyToOne(type => Project, project => project.tasksProject)
  project: Project;
  @Column({
    default: true
  })
  active: boolean;
}
