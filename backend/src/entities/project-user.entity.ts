import { Entity, ManyToOne, Column, PrimaryGeneratedColumn } from 'typeorm';
import { Project } from './project.entity';
import { User } from './user.entity';

@Entity({ name: 'project-user' })
export class ProjectUser {
  @PrimaryGeneratedColumn()
  id: number;
  @ManyToOne(type => User, user => user.projectsUser)
  user: User;
  @ManyToOne(type => Project, project => project.usersProject)
  project: Project;
  @Column({
    default: true
  })
  active: boolean;
}
