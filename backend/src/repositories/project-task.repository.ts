import { EntityRepository } from 'typeorm';
import { BaseRepository } from 'typeorm-transactional-cls-hooked';
import { ProjectTask } from 'entities/project-task.entity';

@EntityRepository(ProjectTask)
export class ProjectTaskRepository extends BaseRepository<ProjectTask> {}
