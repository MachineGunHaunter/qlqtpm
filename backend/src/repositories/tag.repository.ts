import { Tag } from 'entities/tag.entity';
import { EntityRepository } from 'typeorm';
import { BaseRepository } from 'typeorm-transactional-cls-hooked';

@EntityRepository(Tag)
export class TagRepository extends BaseRepository<Tag> {}
