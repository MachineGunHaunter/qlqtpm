import { EntityRepository } from 'typeorm';
import { BaseRepository } from 'typeorm-transactional-cls-hooked';
import { TimeEntries } from 'entities/time-entries.entity';

@EntityRepository(TimeEntries)
export class TimeEntriesRepository extends BaseRepository<TimeEntries> {}
