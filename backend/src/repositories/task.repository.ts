import { EntityRepository } from 'typeorm';
import { BaseRepository } from 'typeorm-transactional-cls-hooked';
import { Task } from 'entities/task.entity';

@EntityRepository(Task)
export class TaskRepository extends BaseRepository<Task> {}
