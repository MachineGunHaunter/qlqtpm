import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import * as cors from 'cors';
import { initializeTransactionalContext } from 'typeorm-transactional-cls-hooked';
import { backend } from '../../config.json';
import { AppModule } from './app.module';

initializeTransactionalContext(); // Initialize cls-hooked

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    logger: console
  });
  app.use(cors());
  const options = new DocumentBuilder()
    .setTitle('Timer')
    .setDescription('The cats API description')
    .setVersion('1.0')
    .addBearerAuth('Authorization', 'header', 'apiKey')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);

  await app.listen(backend.port);
}
bootstrap();
