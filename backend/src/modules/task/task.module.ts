import { Module } from '@nestjs/common';

import { TypeOrmModule } from '@nestjs/typeorm';
import { TaskController } from './controllers/task.controller';
import { TaskService } from './services/task.service';
import { TaskRepository } from 'repositories/task.repository';
import { UserRepository } from 'repositories/user.repository';
import { ProjectUserRepository } from 'repositories/project-user.repository';


@Module({
  imports: [
    TypeOrmModule.forFeature([
      TaskRepository,
      UserRepository,
      ProjectUserRepository
    ]) 
  ],
  controllers: [TaskController],
  providers: [TaskService]
})
export class TaskModule {}

