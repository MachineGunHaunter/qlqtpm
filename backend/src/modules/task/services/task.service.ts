import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Task } from 'entities/task.entity';
import { ProjectUserRepository } from 'repositories/project-user.repository';
import { TaskRepository } from 'repositories/task.repository';
import { UserRepository } from 'repositories/user.repository';
@Injectable()
export class TaskService {
  constructor(
    @InjectRepository(TaskRepository)
    private readonly TaskRepository: TaskRepository,
    @InjectRepository(UserRepository)
    private readonly UserRepository: UserRepository,
    @InjectRepository(ProjectUserRepository)
    private readonly ProjectUserRepository: UserRepository
  ) {}

  //Create Task
  public async getTasksOfProject(projectId: number) {
    const TaskProject = await this.TaskRepository.createQueryBuilder('task')
      .innerJoin('task.projectsTask', 'projectsTask')
      .where('projectsTask.projectId = :id')
      .setParameters({ id: projectId })
      .getMany();
    return TaskProject;
  }

  //Save task
  public async create(task: Task) {
    return await this.TaskRepository.save(task);
  }

  //Select top 5 avtice task in project
  public async Top5Task() {
    //const task = this.TaskRepository.createQueryBuilder('tasks')
    /*lấy ra 5 task được update nhiều nhất:
    -Trong mỗi task, đếm số lần update.
    -SELECT user, MAX(ProjectCount) as MaxProjectCount FROM (SELECT userId, COUNT(userId) AS ProjectCount FROM project-user group by userId) as ProjectCountTable
    -Chọn ra Top 5 các task có số lần update nhiều nhất
    */
    //return await this.TaskRepository.find();
  }
}
