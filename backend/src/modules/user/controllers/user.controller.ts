import { Get, Controller, Body, Post, UseGuards } from '@nestjs/common';
import { ApiUseTags, ApiResponse } from '@nestjs/swagger';
import { User } from 'entities/user.entity';
import { UserService } from '../services/user.service';

@ApiUseTags('users')
@Controller('users')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get()
  @ApiResponse({
    status: 200,
    description: 'Ok'
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async findAll() {
    return await this.userService.findAllUser();
  }

  @Get('/MostUser/')
  @ApiResponse({
    status: 200,
    description: 'Ok'
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async MostUser() {
    return await this.userService.MostUser();
  }
}
