import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ProjectTask } from 'entities/project-task.entity';
import { Task } from 'entities/task.entity';
import { TimeEntries } from 'entities/time-entries.entity';
import { InputCreateTimer } from 'modules/timer/input/InputCreateTimer';
import { mapperGroupTimer } from 'modules/timer/response/TimerReponse';
import { compose, groupBy, map, path, toPairs } from 'ramda';
import { ProjectRepository } from 'repositories/project.repository';
import { TaskRepository } from 'repositories/task.repository';
import { TimeEntriesRepository } from 'repositories/time-entries.repository';
@Injectable()
export class TimerService {
  constructor(
    @InjectRepository(TimeEntriesRepository)
    private readonly timeEntriesRepository: TimeEntriesRepository,
    private readonly projectRepository: ProjectRepository,
    private readonly taskRepository: TaskRepository
  ) {}

  public async startTime(userId: number, inputCreateTimer: InputCreateTimer) {
    const { projectId, description } = inputCreateTimer;
    const project = await this.projectRepository.findOne(projectId);
    const taskFind = await this.taskRepository
      .createQueryBuilder('tasks')
      .innerJoin('project-task', 'project')
      .innerJoin('project-user', 'user')
      .where('tasks.name = :description AND user.userId = :userId', {
        description,
        userId
      })
      .getOne();
    const timerStartingOfUser = await this.timeEntriesRepository
      .createQueryBuilder('time-entries')
      .innerJoinAndSelect('time-entries.project', 'project')
      .innerJoinAndSelect('time-entries.task', 'task')
      .innerJoin('time-entries.user', 'user')
      .where('user.id = :id AND time-entries.isStop = 0', {
        id: userId,
        projectId
      })
      .getOne();
    return this.timeEntriesRepository.manager.transaction(
      async transactionManager => {
        //stop all timer starting
        if (timerStartingOfUser) {
          timerStartingOfUser.isStop = true;
          timerStartingOfUser.stop = new Date();
          await transactionManager.save(timerStartingOfUser);
        }

        let task = taskFind;
        if (!task) {
          task = new Task();
          task.name = description;
          await transactionManager.save(task);

          const taskProject = new ProjectTask();
          taskProject.task = task;
          taskProject.project = project;
          await transactionManager.save(taskProject);
        }
        const timeEntries = new TimeEntries();
        timeEntries.project = project;
        timeEntries.task = task;
        timeEntries.start = new Date();
        timeEntries.userId = userId;
        await transactionManager.save(timeEntries);
      }
    );
  }

  public async stopTime(userId: number) {
    const timerStartingOfUser = await this.timeEntriesRepository
      .createQueryBuilder('time-entries')
      .innerJoinAndSelect('time-entries.project', 'project')
      .innerJoinAndSelect('time-entries.task', 'task')
      .innerJoin('time-entries.user', 'user')
      .where('user.id = :id AND time-entries.isStop = 0', { id: userId })
      .getOne();
    if (timerStartingOfUser) {
      timerStartingOfUser.isStop = true;
      timerStartingOfUser.stop = new Date();
      await this.timeEntriesRepository.save(timerStartingOfUser);
    }
  }

  public async findTimersOfUser(projectId: number) {
    const timers = await this.timeEntriesRepository
      .createQueryBuilder('time-entries')
      .innerJoinAndSelect('time-entries.project', 'project')
      .innerJoinAndSelect('time-entries.task', 'task')
      .innerJoin('time-entries.user', 'user')
      .where(':projectId IS NULL OR project.id = :projectId', {
        projectId
      })
      .orderBy('time-entries.createAt', 'DESC')
      .getMany();
    return compose(
      mapperGroupTimer,
      toPairs,
      map(groupBy(path(['task', 'name']))),
      groupBy((timer: TimeEntries) => {
        return timer.createAt.toDateString();
      })
    )(timers);
  }
}
