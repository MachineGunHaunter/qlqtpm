import { ApiModelProperty } from '@nestjs/swagger';

export class InputCreateTimer {
  @ApiModelProperty()
  projectId: number;

  @ApiModelProperty()
  description: string;
}
