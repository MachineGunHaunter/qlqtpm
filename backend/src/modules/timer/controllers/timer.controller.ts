import {
  Body,
  Controller,
  Get,
  HttpStatus,
  Param,
  Post,
  Res,
  UseGuards
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiResponse, ApiUseTags } from '@nestjs/swagger';
import { JwtPayload } from 'modules/auth/interfaces/jwt-payload.interface';
import { InputCreateTimer } from 'modules/timer/input/InputCreateTimer';
import { GroupTimerReponse } from 'modules/timer/response/TimerReponse';
import { SessionUser } from 'modules/user/user.decorator';
import { TimerService } from '../services/timer.service';

@ApiUseTags('timers')
@Controller('timers')
export class TimerController {
  constructor(private readonly timerService: TimerService) {}

  @Post('start')
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  public async start(
    @SessionUser() payload: JwtPayload,
    @Body() inputCreateTimer: InputCreateTimer,
    @Res() res
  ) {
    await this.timerService.startTime(payload.id, inputCreateTimer);
    res.status(HttpStatus.OK).send();
  }

  @Post('stop')
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  public async stop(@SessionUser() payload: JwtPayload, @Res() res) {
    await this.timerService.stopTime(payload.id);
    res.status(HttpStatus.CREATED).send();
  }

  @Get('/:projectId')
  @UseGuards(AuthGuard('jwt'))
  @ApiResponse({
    status: 200,
    type: GroupTimerReponse,
    isArray: true,
    description: 'Returns Timers'
  })
  @ApiBearerAuth()
  public getTimers(
    @Param('projectId') projectId: number,
    @SessionUser() payload: JwtPayload
  ) {
    return this.timerService.findTimersOfUser(projectId);
  }

  @Get('')
  @UseGuards(AuthGuard('jwt'))
  @ApiResponse({
    status: 200,
    type: GroupTimerReponse,
    isArray: true,
    description: 'Returns Timers'
  })
  @ApiBearerAuth()
  public getTimersOfAllProject(@SessionUser() payload: JwtPayload) {
    return this.timerService.findTimersOfUser(null);
  }
}
