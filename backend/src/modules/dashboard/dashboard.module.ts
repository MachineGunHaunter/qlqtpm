import { Module } from '@nestjs/common';

import { TypeOrmModule } from '@nestjs/typeorm';
import { TimeEntries } from 'entities/time-entries.entity';
import { TimeEntriesRepository } from 'repositories/time-entries.repository';
import { DashboardController } from './controllers/dashboard.controller';
import { DashboardService } from './services/dashboard.service';

@Module({
  imports: [TypeOrmModule.forFeature([TimeEntries, TimeEntriesRepository])],
  controllers: [DashboardController],
  providers: [DashboardService]
})
export class DashboardModule {}
