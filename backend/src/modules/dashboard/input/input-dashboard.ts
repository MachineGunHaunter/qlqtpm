import { ApiModelProperty } from '@nestjs/swagger';

export class InputDashboard {
  @ApiModelProperty({ required: true })
  weekStart: number;
}
