import { ApiModelProperty } from '@nestjs/swagger';
import { duration } from 'moment';

class TimeByProject {
  @ApiModelProperty()
  project: string;
  @ApiModelProperty()
  duration: number;
}

class Activity {
    @ApiModelProperty()
    project: string;
    @ApiModelProperty()
    task: string;
    @ApiModelProperty()
    duration: number;
}

class MostActive {
    @ApiModelProperty()
    userName: string;
    @ApiModelProperty()
    fullName: string;
    @ApiModelProperty()
    totalTime: number;
    @ApiModelProperty()
    activities: Activity[];
}

export class DashboardResponse {
  @ApiModelProperty()
  weekStart: number;

  @ApiModelProperty({ type: Number, isArray: true })
  totalTime: number[];

  @ApiModelProperty({ type: TimeByProject, isArray: true })
  getTimeByProjects: TimeByProject[];

  @ApiModelProperty({type: MostActive})
  mostActive: MostActive
}