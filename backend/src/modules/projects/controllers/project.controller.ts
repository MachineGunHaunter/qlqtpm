import {
  Get,
  Controller,
  Body,
  Post,
  UseGuards,
  Headers,
  Res,
  HttpStatus,
  Put,
  Param,
  Query
} from '@nestjs/common';
import {
  ApiUseTags,
  ApiResponse,
  ApiImplicitQuery,
  ApiBearerAuth,
  ApiImplicitParam,
  ApiImplicitBody
} from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { ProjectService } from '../services/project.service';
import { User } from 'entities/user.entity';
import { SessionUser } from 'modules/user/user.decorator';
import { JwtPayload } from 'modules/auth/interfaces/jwt-payload.interface';
import { Project } from 'entities/project.entity';
import { Task } from 'entities/task.entity';
import { InputUpdateProject } from 'modules/projects/input/InputUpdateProject';
import { ProjectResponse } from 'modules/projects/response/projectResponse';

@ApiUseTags('projects')
@Controller('projects')
export class ProjectController {
  constructor(private readonly projectService: ProjectService) {}

  @Get()
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @ApiResponse({
    status: 200,
    type: ProjectResponse,
    isArray: true,
    description: 'Returns Projects'
  })
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @ApiImplicitQuery({
    name: 'name',
    description: 'Name project',
    required: false,
    type: String
  })
  async findAll(@SessionUser() user: User, @Query('name') name) {
    return await this.projectService.findAllOfUser(user.id, name);
  }

  @Post()
  @ApiResponse({
    status: 200,
    description: 'Ok'
  })
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async create(
    @SessionUser() payload: JwtPayload,
    @Body() project: Project,
    @Res() res
  ) {
    const { id: userId } = payload;
    await this.projectService.createProject(userId, project);
    res.status(HttpStatus.CREATED).send();
  }

  @Put('/:id')
  @ApiResponse({
    status: 200,
    description: 'Ok'
  })
  @UseGuards(AuthGuard('jwt'))
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async update(
    @Param('id') projectId,
    @Body() project: InputUpdateProject,
    @Res() res
  ) {
    await this.projectService.updateProject(projectId, project);
    res.status(HttpStatus.OK).send();
  }
  //- Show Task in project
  //Lay task tham gia project
  @Get('task/:projectId')
  @ApiImplicitParam({ name: 'projectId' })
  @ApiBearerAuth() //cai nay de swagger danh dau la co su dung Bearer auth khi minh goi
  @UseGuards(AuthGuard('jwt')) //cai nay la danh dau enpoint nay co token moi duoc vao vi tao project thi phai co user nen bat buoc phai dang nhap
  @ApiResponse({ status: 201, description: 'Created' }) // de render ra tren giao dien swagger, cuc do
  public getTaskOfProject(@Param('projectId') projectId) {
    return this.projectService.getTasksOfProject(projectId);
  }

  //Create task in project
  @Post('task/CreateTask/:projectId')
  @ApiResponse({
    status: 200,
    description: 'Ok'
  })
  @UseGuards(AuthGuard('jwt'))
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @ApiImplicitParam({ name: 'projectId' })
  @ApiBearerAuth()
  async createTaskInProject(
    @SessionUser() payload: JwtPayload,
    @Param('projectId') projectId,
    @Body() task: Task,
    @Res() res
  ) {
    await this.projectService.createTaskInProject(projectId, task);
    res.status(HttpStatus.CREATED).send();
  }

  //Remove task in project
  @Post('task/RemoveTask/:projectId&:taskId')
  @ApiResponse({
    status: 200,
    description: 'Ok'
  })
  @UseGuards(AuthGuard('jwt'))
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @ApiImplicitParam({ name: 'projectId' })
  @ApiImplicitParam({ name: 'taskId' })
  @ApiBearerAuth()
  async removeTaskInProject(
    @SessionUser() payload: JwtPayload,
    @Param('projectId') projectID,
    @Param('taskId') taskID,
    //@Body() project: Project,
    //@Body() task: Task,
    @Res() res
  ) {
    await this.projectService.removeTaskInProject(projectID, taskID);
    res.status(HttpStatus.CREATED).send();
  }
}
