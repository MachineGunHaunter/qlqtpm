import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { UserService } from '../../user/services/user.service';
import { JwtService } from '@nestjs/jwt';
import { hash, compare } from 'bcrypt';
import { UserLogin } from '../interfaces/user-login.interface';
import { JwtPayload } from '../interfaces/jwt-payload.interface';
import { User } from 'entities/user.entity';
import { UserRepository } from 'repositories/user.repository';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(UserRepository)
    private readonly userRepository: UserRepository,
    private readonly jwtService: JwtService
  ) {}

  async signIn({ userName, password }: UserLogin): Promise<any> {
    const user = await this.userRepository.findOne({
      where: { userName }
    });
    if (!user) {
      throw new HttpException('User not found', HttpStatus.BAD_REQUEST);
    }
    if (!(await compare(password, user.password))) {
      throw new HttpException('Password not match', HttpStatus.BAD_REQUEST);
    }
    const userPayload: JwtPayload = { email: user.email, id: user.id };
    const access_token = this.jwtService.sign(userPayload);
    return {
      access_token,
      fullName: user.fullName,
      userName: user.userName,
      email: user.email,
      avatar: user.avatar
    };
  }

  async register(user: User) {
    const userFind = await this.userRepository.findOne({
      where: [{ userName: user.userName }, { email: user.email }]
    });
    if (userFind) {
      throw new HttpException(
        'UserName or email are exists',
        HttpStatus.BAD_REQUEST
      );
    }
    const { password } = user;
    const hashPassword = await hash(password, 10);
    await this.userRepository.save({ ...user, password: hashPassword });
    return true;
  }
}
