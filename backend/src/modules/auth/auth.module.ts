import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';

import { UserModule } from '../user/user.module';
import { AuthController } from './controllers/auth.controller';
import { AuthService } from './services/auth.service';
import { JwtStrategy } from './passport/jwt.strategy';
import { UserService } from 'modules/user/services/user.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'entities/user.entity';
import { UserRepository } from 'repositories/user.repository';

@Module({
  imports: [
    PassportModule.register({
      defaultStrategy: 'jwt',
      property: 'profile',
      session: true
    }),
    JwtModule.register({
      secretOrPrivateKey: 'ce656850400574e9f9cffb285ee8abc0',
      signOptions: {
        expiresIn: "7d"
      }
    }),
    TypeOrmModule.forFeature([User, UserRepository])
  ],
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy]
})
export class AuthModule {}
