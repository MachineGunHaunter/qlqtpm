import { ApiModelProperty } from '@nestjs/swagger';

class TimeByTask {
  @ApiModelProperty()
  task: string;
  @ApiModelProperty()
  duration: number;
}

export class ReportTimerSumaryTaskResponse {
  @ApiModelProperty()
  weekStart: number;

  @ApiModelProperty({ type: Number, isArray: true })
  totalTime: number[];

  @ApiModelProperty({ type: TimeByTask, isArray: true })
  getTimeByTasks: TimeByTask[];
}
