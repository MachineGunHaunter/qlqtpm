import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiResponse, ApiUseTags } from '@nestjs/swagger';
import { InputReportsSumary } from '../input/input-reports-sumary';
import { ReportTimerSumaryResponse } from '../response/reponse-report-sumary';
import { ReportTimerSumaryTaskResponse } from '../response/reponse-report-task';
import { ReportService } from '../services/report.service';

@ApiUseTags('reports')
@Controller('reports')
export class ReportController {
  constructor(private readonly reportService: ReportService) {}

  @Post('/sumary-project')
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @ApiResponse({
    status: 200,
    type: ReportTimerSumaryResponse,
    description: 'Returns Projects'
  })
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  async reportSumaryForProject(@Body() queries: InputReportsSumary) {
    return await this.reportService.getReportSumary(queries);
  }

  @Post('/sumary-task')
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @ApiResponse({
    status: 200,
    type: ReportTimerSumaryTaskResponse
  })
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  async reportSumaryForTask(@Body() queries: InputReportsSumary) {
    return await this.reportService.getReportSumaryForTask(queries);
  }
}
