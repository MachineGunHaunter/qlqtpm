import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TimeEntries } from 'entities/time-entries.entity';
import {
  compose,
  groupBy,
  head,
  map,
  merge,
  path,
  prop,
  reduce,
  values
} from 'ramda';
import { TimeEntriesRepository } from 'repositories/time-entries.repository';
import { InputReportsSumary } from '../input/input-reports-sumary';
import moment = require('moment');
@Injectable()
export class ReportService {
  constructor(
    @InjectRepository(TimeEntriesRepository)
    private readonly timeEntriesRepository: TimeEntriesRepository
  ) {}

  public async getReportSumary(queries: InputReportsSumary) {
    const timers = await this.getDataReport(queries);

    const getToTalTimeGroupByDate = this.getToTalTimeGroupByDate(
      queries.weekStart,
      timers
    );

    const getTimeByProjects = compose(
      map(value => {
        return {
          projectName: compose(
            path(['project', 'name']),
            head
          )(value),
          duration: this.getTotalDuration(value)
        };
      }),
      values,
      groupBy(path(['project', 'id']))
    )(timers);
    return {
      weekStart: queries.weekStart,
      totalTime: getToTalTimeGroupByDate,
      getTimeByProjects
    };
  }

  async getReportSumaryForTask(queries: InputReportsSumary) {
    const timers = await this.getDataReport(queries);

    const getToTalTimeGroupByDate = this.getToTalTimeGroupByDate(
      queries.weekStart,
      timers
    );
    const getTimeByProjects = map(value => {
      const from = new Date(prop('start')(value)).getTime();
      const to = new Date(prop('stop')(value)).getTime();

      return {
        task: path(['task', 'name'])(value),

        user: path(['user', 'fullName'])(value),
        from,
        to,
        duration: to - from
      };
    })(timers);
    return {
      weekStart: queries.weekStart,
      totalTime: getToTalTimeGroupByDate,
      getTimeByProjects
    };
  }

  getTotalDuration(dataTineStamp) {
    return reduce((acc, value) => {
      return value.stop ? acc + moment(value.stop).diff(value.start) : acc;
    }, 0)(dataTineStamp);
  }

  getToTalTimeGroupByDate(weekStart, data) {
    const timeWeekStart = new Date(weekStart);

    const dayQuery = moment(timeWeekStart);
    const now = moment(timeWeekStart).add(6, 'days');
    const timersInitForDays = this.enumerateDaysBetweenDates(dayQuery, now);
    return compose(
      map(this.getTotalDuration),
      values,
      merge(timersInitForDays),
      groupBy((timer: TimeEntries) => {
        return moment(timer.createAt).format('M/D/YYYY');
      })
    )(data);
  }

  async getDataReport(queries: InputReportsSumary) {
    const searchTearm = queries.searchTearm
      ? '%' + queries.searchTearm + '%'
      : null;
    const projects = queries.projects.length > 0 ? queries.projects : null;
    const teams = queries.teams.length > 0 ? queries.teams : null;
    const timeWeekStart = new Date(queries.weekStart);
    let timersQuery = this.timeEntriesRepository
      .createQueryBuilder('timeEntries')
      .leftJoinAndSelect('timeEntries.task', 'task')
      .leftJoinAndSelect('timeEntries.project', 'project')
      .leftJoinAndSelect('timeEntries.user', 'user')
      .leftJoin('user.team', 'team')
      .where(
        `(:startTime IS NULL OR (timeEntries.createAt >= :startTime AND timeEntries.createAt <= :endTime)  )
        AND (:searchTearm IS NULL OR task.name LIKE :searchTearm OR project.name LIKE :searchTearm )
        AND timeEntries.isStop = 1`,
        {
          startTime: moment(timeWeekStart).toDate(),
          endTime: moment(timeWeekStart)
            .add(6, 'days')
            .toDate(),
          searchTearm
        }
      );
    if (projects) {
      timersQuery = timersQuery.andWhere(
        `timeEntries.projectId IN(:...projects)`,
        { projects }
      );
    }
    if (teams) {
      timersQuery = timersQuery.andWhere(`team.id IN(:...teams)`, { teams });
    }

    return timersQuery.orderBy('timeEntries.createAt', 'DESC').getMany();
  }

  enumerateDaysBetweenDates(startDate, endDate) {
    var now = startDate,
      dates = {};
    while (now.isSameOrBefore(endDate)) {
      dates[now.format('M/D/YYYY')] = [0];
      now.add(1, 'days');
    }
    return dates;
  }
}
