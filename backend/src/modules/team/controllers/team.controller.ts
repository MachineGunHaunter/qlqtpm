import {
  ApiUseTags,
  ApiResponse,
  ApiBearerAuth,
  ApiImplicitParam
} from '@nestjs/swagger';
import {
  Controller,
  Body,
  Post,
  Guard,
  UseGuards,
  Res,
  HttpStatus,
  Put,
  Param,
  Get,
  Delete
} from '@nestjs/common';
import { TeamService } from '../services/team.service';
import { get } from 'http';
import { Team } from 'entities/team.entity';
import { AuthGuard } from '@nestjs/passport';

@ApiUseTags('team') //cái này định nghĩa cho swaggẻ render ra cái tags name
@Controller('team') //dịnh nghĩa tên controller
export class TeamController {
  constructor(private readonly teamService: TeamService) {}

  //Lay team tham gia project
  @Get('GetTeamOfProject/:projectId')
  @ApiImplicitParam({ name: 'projectId' })
  @ApiBearerAuth() //cai nay de swagger danh dau la co su dung Bearer auth khi minh goi
  @UseGuards(AuthGuard('jwt')) //cai nay la danh dau enpoint nay co token moi duoc vao vi tao project thi phai co user nen bat buoc phai dang nhap
  @ApiResponse({ status: 201, description: 'Created' }) // de render ra tren giao dien swagger, cuc do
  public getTeamsOfProject(@Param('projectId') projectId) {
    return this.teamService.getTeamsOfProject(projectId);
  }

  //tao team
  @Post() // dinh nghia phuong thuc post neu muon theem url
  @ApiBearerAuth() //cai nay de swagger danh dau la co su dung Bearer auth khi minh goi
  @UseGuards(AuthGuard('jwt')) //cai nay la danh dau enpoint nay co token moi duoc vao vi tao project thi phai co user nen bat buoc phai dang nhap
  @ApiResponse({ status: 201, description: 'Created' }) // de render ra tren giao dien swagger, cuc do
  public async createProject(
    @Res() res,
    //@SessionUser() userPayloadToken: JwtPayload,
    @Body() team: Team
  ) {
    await this.teamService.create(team);
    res.status(HttpStatus.CREATED).send(); // de gui reponse status 201 ve khi tao project thanh cong
  }

  //remove member
  @Post('RemoveMember/:userId')
  @ApiImplicitParam({ name: 'userId' })
  @ApiBearerAuth() //cai nay de swagger danh dau la co su dung Bearer auth khi minh goi
  @UseGuards(AuthGuard('jwt')) //c
  public async removeMember(@Res() res, @Param('userId') userId) {
    await this.teamService.RemoveMember(userId);
    res.status(HttpStatus.OK).send();
  }

  //change permission
  @Post('ChangePermission/:teamId')
  @ApiImplicitParam({ name: 'teamId' })
  @ApiBearerAuth() //cai nay de swagger danh dau la co su dung Bearer auth khi minh goi
  @UseGuards(AuthGuard('jwt')) //c
  public async changePermission(@Res() res, @Param('teamId') teamId) {
    await this.teamService.ChangePermission(teamId);
    res.status(HttpStatus.OK).send();
  }
}
