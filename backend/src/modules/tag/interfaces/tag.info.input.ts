import { ApiModelProperty } from '@nestjs/swagger';

export class TagInfo {
  @ApiModelProperty()
  name: string;
}
