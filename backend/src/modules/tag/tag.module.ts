import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Tag } from 'entities/tag.entity';
import { TagRepository } from 'repositories/tag.repository';
import { TagController } from './controllers/tag.controller';
import { TagService } from './services/tag.service';

@Module({
  imports: [TypeOrmModule.forFeature([TagRepository, Tag])],
  controllers: [TagController],
  providers: [TagService]
})
export class TagModule {}
