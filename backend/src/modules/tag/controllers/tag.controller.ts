import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Put,
  Res,
  UseGuards
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiBearerAuth,
  ApiImplicitParam,
  ApiResponse,
  ApiUseTags
} from '@nestjs/swagger';
import { TagInfo } from '../interfaces/tag.info.input';
import { TagService } from '../services/tag.service';

@ApiUseTags('tag')
@Controller('tag')
export class TagController {
  constructor(private readonly tagService: TagService) {}

  //Comment ten cua task làm truoc ham
  @Get()
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiResponse({ status: 201, description: 'Created' })
  public getTag() {
    return this.tagService.getTags();
  }

  //Comment ten cua task làm truoc ham
  @Get('/:tagId')
  @ApiImplicitParam({ name: 'tagId' })
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiResponse({ status: 201, description: 'Created' })
  public getTagById(@Param('tagId') tagId) {
    return this.tagService.getTagsById(tagId);
  }

  @Post('')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiResponse({ status: 201, description: 'Created' })
  public createTag(@Body() tag: TagInfo) {
    return this.tagService.create(tag);
  }

  //Comment ten cua task làm truoc ham
  @Put('/:tagId')
  @ApiImplicitParam({ name: 'tagId' })
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiResponse({ status: 201, description: 'Created' })
  public async updateTag(
    @Res() res,
    @Param('tagId') tagId,
    @Body() task: TagInfo
  ) {
    if (!tagId) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: 'tagId is a required'
        },
        400
      );
    }
    await this.tagService.update(tagId, task);
    res.status(HttpStatus.OK).send();
  }
  //Comment ten cua task làm truoc ham
  @Delete('/:tagId')
  @ApiImplicitParam({ name: 'tagId' })
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiResponse({ status: 201, description: 'Created' })
  public async deleteTag(@Res() res, @Param('tagId') tagId) {
    if (!tagId) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: 'tagId is a required'
        },
        400
      );
    }
    await this.tagService.delete(tagId);
    res.status(HttpStatus.OK).send();
  }
}
