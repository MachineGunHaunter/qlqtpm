import {
  ApiUseTags,
  ApiResponse,
  ApiBearerAuth,
  ApiImplicitParam
} from '@nestjs/swagger';
import {
  Controller,
  Body,
  Post,
  Guard,
  UseGuards,
  Res,
  HttpStatus,
  Put,
  Param,
  Get
} from '@nestjs/common';
import { TestService } from '../services/test.service';
import { Project } from 'entities/project.entity';
import { AuthGuard } from '@nestjs/passport';
import { SessionUser } from 'modules/user/user.decorator';
import { JwtPayload } from 'modules/auth/interfaces/jwt-payload.interface';
import { InputUpdateProject } from 'dto/InputModels/InputUpdateProject';

@ApiUseTags('test') //cái này định nghĩa cho swaggẻ render ra cái tags name
@Controller('test') //dịnh nghĩa tên controller
export class TestController {
  constructor(private readonly testService: TestService) {}

  @Post() // dinh nghia phuong thuc post neu muon theem url
  @ApiBearerAuth() //cai nay de swagger danh dau la co su dung Bearer auth khi minh goi
  @UseGuards(AuthGuard('jwt')) //cai nay la danh dau enpoint nay co token moi duoc vao vi tao project thi phai co user nen bat buoc phai dang nhap
  @ApiResponse({ status: 201, description: 'Created' }) // de render ra tren giao dien swagger, cuc do
  //SessionUser de lay ra cai payload duoc dinh kem trong jwt, payload thi dinh nghia o day khi nguoi dung login
  //Body la danh dau cai bien nay se la cuc json trong request cua minh gui len
  public async createProject(
    @Res() res,
    @SessionUser() userPayloadToken: JwtPayload,
    @Body() project: Project
  ) {
    await this.testService.createProject(userPayloadToken.id, project);
    res.status(HttpStatus.CREATED).send(); // de gui reponse status 201 ve khi tao project thanh cong
  }

  @Put('/:projectId')
  @ApiImplicitParam({ name: 'projectId' })
  @ApiBearerAuth() //cai nay de swagger danh dau la co su dung Bearer auth khi minh goi
  @UseGuards(AuthGuard('jwt')) //c
  public async updateProject(
    @Res() res,
    @Param('projectId') projectId,
    @Body() projectUpdate: InputUpdateProject
  ) {
    await this.testService.updateProject(projectId, projectUpdate);
    res.status(HttpStatus.OK).send();
  }

  @Get('/:projectId')
  @ApiImplicitParam({ name: 'projectId' })
  public getUsersOfProject(@Param('projectId') projectId) {
    return this.testService.getUserOfProject(projectId);
  }
}
