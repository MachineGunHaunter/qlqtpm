import { Module } from '@nestjs/common';

import { TypeOrmModule } from '@nestjs/typeorm';
import { ProjectRepository } from '../../repositories/project.repository';
import { Project } from '../../entities/project.entity';
import { User } from 'entities/user.entity';
import { UserRepository } from 'repositories/user.repository';
import { TestController } from './controllers/test.controller';
import { TestService } from './services/test.service';
import { ProjectUser } from 'entities/project-user.entity';
import { ProjectUserRepository } from 'repositories/project-user.repository';

//dang ky constroller va service
@Module({
  imports: [
    TypeOrmModule.forFeature([
      Project,
      ProjectRepository,
      User, // vi repository can 1 entity do minh dinh nghia o day nen dong thoi inject entity tuong ung
      UserRepository,
      ProjectUserRepository,
      ProjectUser
    ]) //cai import nay su dung de inject cac cai ma minh can, cu the o day trong file
    //service co su dung UserRepository, ProjectRepository nen phai inject vao
  ],
  controllers: [TestController],
  providers: [TestService]
})
export class TestModule {}
//khi chay mode dev thi khi source thay doi no tu dong refresh lai
