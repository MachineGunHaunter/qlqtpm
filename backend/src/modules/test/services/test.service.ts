import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ProjectRepository } from 'repositories/project.repository';
import { Project } from 'entities/project.entity';
import { InputUpdateProject } from 'dto/InputModels/InputUpdateProject';
import { UserRepository } from 'repositories/user.repository';
import { ProjectUser } from 'entities/project-user.entity';
import { ProjectUserRepository } from 'repositories/project-user.repository';
@Injectable()
export class TestService {
  constructor(
    @InjectRepository(ProjectRepository)
    private readonly projectRepository: ProjectRepository,
    @InjectRepository(UserRepository)
    private readonly userRepository: UserRepository,
    @InjectRepository(ProjectUserRepository)
    private readonly projectUserRepository: ProjectUserRepository
  ) {}

  //ở đây cái input này có thhể là cái model hoặc 1 dto mà mình tự định
  //create co them thong tin user nen o day them userid
  public async createProject(userId: number, project: Project) {
    const user = await this.userRepository.findOne(userId);
    // oday dong thoi insert vao 2 bang nen phai mo transaction 1 bang la user va 1 bang la project
    return this.projectRepository.manager.transaction(
      async transactionManager => {
        // insert vao bang project
        console.log(project);

        await transactionManager.save(Project, project);
        //dong thoi phai insert vao bang user project
        //co the tao constructor roi nhet vao luon
        const newRecordUserProject = new ProjectUser();
        newRecordUserProject.user = user;
        newRecordUserProject.project = project;
        //sau do luu lai
        return transactionManager.save(newRecordUserProject);
      }
    );
  }

  public async updateProject(
    projectId: number,
    projectUpdate: InputUpdateProject
  ) {
    const findProject = await this.projectRepository.findOne(projectId);
    return await this.projectRepository.update(
      { id: projectId },
      { ...findProject, ...projectUpdate }
    );
  }

  public async getUserOfProject(projectId: number) {
    const projectUser = await this.projectUserRepository.find({
      relations: ['user'],
      where: { projectId }
    });
    console.log(projectUser);
    return projectUser;
  }
}
