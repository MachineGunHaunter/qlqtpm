import { MigrationInterface, QueryRunner } from 'typeorm';
import { Project } from 'entities/project.entity';
import { User } from 'entities/user.entity';
import { Task } from 'entities/task.entity';

export class Project1641602735335 implements MigrationInterface {
  async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(User)
      .values([
        {
          fullName: 'admin',
          email: 'admin.com.vn',
          userName: 'admin',
          password:
            '$2b$10$kpBSP1bw9PDKMw.MTIAjQuyOeBTV05AqeO.N/onPxQqUPtxpx6YXq'
        } as User
      ])
      .execute();
    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(Project)
      .values([
        {
          name: 'project1'
        } as Project
      ])
      .execute();
    await queryRunner.manager
      .createQueryBuilder()
      .insert()
      .into(Task)
      .values([
        {
          name: 'task1'
        } as Task
      ])
      .execute();
  }

  async down(queryRunner: QueryRunner): Promise<any> {}
}
