## Project vísion

- Các vấn đề mà sản phẩm giải quyết:
    - Thông tin về thời gian được ghi lại không chính xác do không có công cụ đo lường thời gian hỗ trợ. Do đó cần phải tăng tính chính xác cho việc quản lý thời gian bằng cách đưa tính năng quản lý thời gian cho các tác vụ.
    - Việc tổng hợp, thống kê trong quá trình thực hiện các tác vụ là cần thiết, nhưng chưa có công cụ hỗ trợ cho vấ đề này. Mục tiêu là cần phải có những bản thống kê từ thông tin về quản lý thời gian đã được lưu trữ, cung cấp những tính năng thống kê, tích hợp biểu đồ, thể hiện các kết quả một cách rõ ràng, chính xác
    - Ngoài việc chỉ quản lí thời gian thực hiện tác vụ, các thông tin mở rộng liên quan đến tác vụ chưa được quản lý chi tiết như: 
        - Tác vụ đó thuộc Team/Cá nhân nào thực hiện
        - Tác vụ đó thuộc dự án nào
        - Trong Team gồm có những thành viên nào
        - Trong một dự án bao gồm những Team nào

- Phạm vi phần mềm:
    - Tất cả mọi cá nhân, nhóm, tổ chức có nhu cầu ghi lại, phân tích quá trình làm việc.
	
- Bussiness Case:
    - Các thành viên nhóm phát triển dự án website “ListenMp3” trong quá trình thực hiện dự án muốn ghi chú lại tiến độ công việc cũng như thời gian mà các thành viên sử dụng để thực hiện các công việc (task) được giao. Qua đó, có một cái nhìn tổng quát về quá trình hoạt động của nhóm để đưa ra các chiến lượt phát triển kịp thời. Tất cả các hoạt động này chỉ được ghi chú dưới dạng file .doc, .xls. Nhận thấy việc ghi chú hiện tại gây mất nhiều thời gian, độ chính xác không cao, tốn nhiều không gian lưu trữ, khó truy xuất dữ liệu dẫn đến việc thống kê các kết quả khó có thể thực hiện. Leader của nhóm – anh Trung quyết định sử dụng ứng dụng “Website LogWork” để áp dụng các biện pháp quản lý tác vụ cho nhóm. Qua sử dụng, anh nhận thấy ứng dụng đem lại hiệu quả cao trong công tác quản lý và theo dõi thời gian làm việc của nhóm, từ đó vạch ra lộ trình giúp tăng hiệu suất làm việc của các thành viên, và việc thống kê tiến độ công việc cũng như thời gian thực hiện dự án cũng được thể hiện rõ ràng, chính xác.

- Sản phẩm liên quan
    - toggl.com
        - Là hệ thống time tracking online, có thể dùng cho cá nhân hoặc 1 nhóm
	    - UI/UX tương đối dễ sử dụng
        - Các tính năng vẫn còn hạn chế trong bản miễn phí.
    - track.timeneye.com
        - Cách sử dụng chia rõ ra theo workspace
        - Giao diện mang tính chuyên nghiệp hơn toggl
        - Thích hợp với các công ty quy mô lớn.
        - Các tính năng đặc biệt chỉ có trong bản trả phí
    - tmetric.com
    - ...
- Thời gian thực hiện dự kiến:
    - 10 tuần

- Môi trường phát triển
    - Ngôn ngữ chủ yếu: NodeJS, PHP, VueJS
	- Nền tảng: Web application
	- Database: MySQL

- Viễn cảnh tương lai:
    - Giải pháp cung cấp khả năng quan sát, phân tích tiến trình thực hiện công việc.
    - Việc quản lý, theo dõi thời gian làm việc trở nên đơn giản, nhanh chóng.
    - Tích hợp ứng dụng cho các thiết bị di động tạo sự linh hoạt cho người dùng.
    - Trở thành một công cụ quản lý thời gian không thể thiếu cho mỗi cá nhân, tổ chức.