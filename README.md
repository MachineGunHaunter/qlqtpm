require installed: python, nodejs, mysql

 run commands are:
```
npm i -g typescript
npm i -g ts-node 
npm install
```
change username, password, database in file ormconfig.json with your mysql enviroment
run file migrate with command:
```
npm run migrate
```

run server with command:
```
npm start
```

run server for development with command:
```
npm run start:dev (required install global nodemon )
```

Go to browser open swaggerUI: http://localhost:3000/api/
