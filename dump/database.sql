-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 16, 2018 at 08:23 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `database`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(11) NOT NULL,
  `timestamp` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `timestamp`, `name`) VALUES
(1, 1542116122471, 'migrate1542116122471'),
(2, 1641602735335, 'Project1641602735335');

-- --------------------------------------------------------

--
-- Table structure for table `project-task`
--

CREATE TABLE `project-task` (
  `id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `taskId` int(11) DEFAULT NULL,
  `projectId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `project-user`
--

CREATE TABLE `project-user` (
  `id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `userId` int(11) DEFAULT NULL,
  `projectId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `project-user`
--

INSERT INTO `project-user` (`id`, `active`, `userId`, `projectId`) VALUES
(1, 1, 1, 2),
(2, 1, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `name` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `created-at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated-at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `name`, `created-at`, `updated-at`) VALUES
(1, 'project1', '2018-11-14 14:10:31', '2018-11-14 14:10:31'),
(2, '', '2018-11-14 15:46:37', '2018-11-14 15:46:37'),
(3, '', '2018-11-14 15:47:01', '2018-11-14 15:47:01');

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `id` int(11) NOT NULL,
  `name` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `created-at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated-at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `name`, `created-at`, `updated-at`, `active`) VALUES
(1, 'task1', '2018-11-14 14:10:31', '2018-11-14 14:10:31', 1);

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `id` int(11) NOT NULL,
  `title` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created-at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated-at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `time-entries`
--

CREATE TABLE `time-entries` (
  `id` int(11) NOT NULL,
  `start` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `stop` datetime DEFAULT NULL,
  `lastStart` datetime DEFAULT NULL,
  `create-at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `duration` int(11) DEFAULT NULL,
  `bill` int(11) DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `isStop` tinyint(4) NOT NULL DEFAULT '0',
  `taskId` int(11) DEFAULT NULL,
  `projectId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `full-name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user-name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `teamId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `full-name`, `email`, `user-name`, `avatar`, `password`, `active`, `teamId`) VALUES
(1, 'admin', 'admin.com.vn', 'admin', NULL, '$2b$10$kpBSP1bw9PDKMw.MTIAjQuyOeBTV05AqeO.N/onPxQqUPtxpx6YXq', 1, NULL),
(2, 'Thien Truong', 'truongtichthien@g.c', 'ttthien', 'img/annika.jpg', '$2b$10$Pqu.g2zAaCzbT5IBM2gkQuTrTwmCAM82k7ltPYjXkmRc4BuIxogUe', 1, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project-task`
--
ALTER TABLE `project-task`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_d6117337d5f04affd8acfa7a4a9` (`taskId`),
  ADD KEY `FK_591dbf277b56eb1db89f878e135` (`projectId`);

--
-- Indexes for table `project-user`
--
ALTER TABLE `project-user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_7c36804f184eda128cba3978dd9` (`userId`),
  ADD KEY `FK_d39ae16e4c3f08704ee8a49404a` (`projectId`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `time-entries`
--
ALTER TABLE `time-entries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_cd24746bc152daf8dbd2f3a18ba` (`taskId`),
  ADD KEY `FK_f4cda2319a5a006d2b0b120efb2` (`projectId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_d1803064187c8f38e57a9c4984c` (`teamId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `project-task`
--
ALTER TABLE `project-task`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `project-user`
--
ALTER TABLE `project-user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `time-entries`
--
ALTER TABLE `time-entries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `project-task`
--
ALTER TABLE `project-task`
  ADD CONSTRAINT `FK_591dbf277b56eb1db89f878e135` FOREIGN KEY (`projectId`) REFERENCES `projects` (`id`),
  ADD CONSTRAINT `FK_d6117337d5f04affd8acfa7a4a9` FOREIGN KEY (`taskId`) REFERENCES `tasks` (`id`);

--
-- Constraints for table `project-user`
--
ALTER TABLE `project-user`
  ADD CONSTRAINT `FK_7c36804f184eda128cba3978dd9` FOREIGN KEY (`userId`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK_d39ae16e4c3f08704ee8a49404a` FOREIGN KEY (`projectId`) REFERENCES `projects` (`id`);

--
-- Constraints for table `time-entries`
--
ALTER TABLE `time-entries`
  ADD CONSTRAINT `FK_cd24746bc152daf8dbd2f3a18ba` FOREIGN KEY (`taskId`) REFERENCES `tasks` (`id`),
  ADD CONSTRAINT `FK_f4cda2319a5a006d2b0b120efb2` FOREIGN KEY (`projectId`) REFERENCES `projects` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `FK_d1803064187c8f38e57a9c4984c` FOREIGN KEY (`teamId`) REFERENCES `teams` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
