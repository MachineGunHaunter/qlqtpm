(function (axios, _) {
  'use strict';

  Vue.component('lw-team', function (resolve, reject) {
    axios
      .get('./templates/team.html')
      .then(function (template) {
        resolve({
          data: function () {
            return {
              teamMembers: [],
              sortBy: '',
              sortOrder: '',
              searchKey: ''
            }
          },
          methods: {
            getData: function () {
              var self = this;
              axios
                .get('/users')
                .then(function (response) {
                  var resData = _.get(response, 'data');

                  self.teamMembers = _.cloneDeep(resData);
                  self.teamMembersSorted = _.cloneDeep(self.teamMembers);
                })
                .catch(function (error) {

                });
            },
            sortTable: function (sortKey) {
              if (this.sortBy === sortKey) {
                this.sortOrder = (this.sortOrder === 'asc' ? 'desc' : (this.sortOrder === 'desc' ? '' : 'asc'));
              } else {
                this.sortOrder = 'asc';
                this.sortBy = sortKey;
              }
            }
          },
          computed: {
            teamMembersSorted: function () {
              var self = this;
              var matchedItem = [];

              if (self.searchKey !== '') {
                matchedItem = _.filter(self.teamMembers, function (mem) {
                  return mem.fullName.indexOf(self.searchKey) >= 0
                    || mem.email.indexOf(self.searchKey) >= 0;
                });
              } else {
                matchedItem = _.cloneDeep(self.teamMembers);
              }

              if (!self.sortOrder) {
                self.sortBy = '';
                return matchedItem;
              }
              return _.orderBy(matchedItem, self.sortBy, self.sortOrder);
            }
          },
          mounted: function () {
            this.getData();
          },
          template: template.data
        });
      })
      .catch(function (error) {
        reject('ERROR: ', error);
      });
  });

})(window.axios, window._);