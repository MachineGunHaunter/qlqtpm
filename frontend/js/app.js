function showSearchOptions(toggler, options) {
  options.style.display = "none";
  options.style.backgroundColor = "white";
  options.style.zIndex = 10040;

  toggler.addEventListener("click", function(e) {
    if (options.style.display == "none") {
      let rect = toggler.getBoundingClientRect();
      options.style.position = "absolute";
      options.style.top = `${rect.top - 11}px`;
      options.style.left = `${rect.left}px`;
      options.style.display = "block";
    } else {
      options.style.display = "none";
    }
  });
}

function hideSearchOptions(optionsId) {
  let mouse_is_inside = false;

  $(document).ready(function() {
    $(`#${optionsId}`).hover(
      function() {
        mouse_is_inside = true;
      },
      function() {
        mouse_is_inside = false;
      }
    );

    $("body").mouseup(function() {
      if (!mouse_is_inside) $(`#${optionsId}`).hide();
    });
  });
}

function setAllCheckBox(checkboxId) {
  let chk = document.querySelector(`#${checkboxId}`);
  chk.addEventListener("click", function(e) {
    if (e.target.checked === true) {
      document
        .querySelectorAll(`[id^="${checkboxId}"]`)
        .forEach(function(item) {
          item.checked = true;
        });
    } else {
      document
        .querySelectorAll(`[id^="${checkboxId}"]`)
        .forEach(function(item) {
          item.checked = false;
        });
    }
  });
}
