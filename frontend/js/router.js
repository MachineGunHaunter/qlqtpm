(function ($, axios) {
  'use strict';

  /** component defined in timer.js */
  var timer = {template: '<lw-timer></lw-timer>'};
  /** component defined in dashboard.js */
  var dashboard = {template: '<lw-dashboard></lw-dashboard>'};
  /** component defined in reports.js */
  var reportsSummary = {template: '<lw-reports-summary></lw-reports-summary>'};
  var reportsDetailed = {template: '<lw-reports-detailed></lw-reports-detailed>'};
  /** component defined in projects.js */
  var projects = {template: '<lw-projects></lw-projects>'};
  /** component defined in teams.js */
  var team = {template: '<lw-team></lw-team>'};
  /** component defined in tags.js */
  var tags = {template: '<lw-tags></lw-tags>'};


  var routes = [
    {path: '/timer', component: timer},
    {path: '/dashboard', component: dashboard},
    {path: '/reports', redirect: '/reports/summary'},
    {path: '/reports/summary', component: reportsSummary},
    {path: '/reports/detailed', component: reportsDetailed},
    {path: '/projects', component: projects},
    {path: '/team', component: team},
    {path: '/tags', component: tags},

    {path: '*', redirect: '/timer'}
  ];

  var router = new VueRouter({routes: routes});

  // new Vue({router}).$mount('#log-work');

  new Vue({
    el: '#log-work',
    data: {},
    computed: {
      fullName: function () {
        return document.cookie.replace(/(?:(?:^|.*;\s*)fullName\s*\=\s*([^;]*).*$)|^.*$/, "$1");
      },
      email: function () {
        return document.cookie.replace(/(?:(?:^|.*;\s*)email\s*\=\s*([^;]*).*$)|^.*$/, "$1");
      },
      avatar: function () {
        return document.cookie.replace(/(?:(?:^|.*;\s*)avatar\s*\=\s*([^;]*).*$)|^.*$/, "$1");
      }
    },
    router,
    methods: {
      logout: function () {
        document.cookie = 'accessToken=';
        window.location.href = '/login';
      }
    }
  });

})($, window.axios);