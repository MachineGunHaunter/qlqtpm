(function (axios, _, moment, Chart, $, jsPDF, html2canvas) {
  'use strict';

  function getColorCode() {
    return ['#3e95cd', '#8e5ea2', '#3cba9f', '#e8c3b9', '#c45850'];
  }

  function convertDuration(duration, format) {
    return moment.utc(moment.duration(duration).asMilliseconds()).format(format);
  }

  function generateBartChartConfig() {
    return {
      legend: {
        display: false
      },
      scales: {
        yAxes: [
          {
            ticks: {
              beginAtZero: true,
              callback: function (value) {
                return convertDuration(value, 'HH') + ' h';
              }
            }
          }
        ]
      },
      tooltips: {
        callbacks: {
          title: function (tooltipItem, data) {
            return data['labels'][tooltipItem[0]['index']][1];
          },
          label: function (tooltipItem, data) {
            var label = data['datasets'][0].label;
            var duration = data['datasets'][0]['data'][tooltipItem['index']] || "";
            if (duration) {
              return label + convertDuration(duration, 'HH:mm:ss');
            }
          }
        }
      }
    };
  }

  function generatePieChartConfig() {
    return {
      legend: {
        display: false
      },
      elements: {
        center: {
          color: '#000000',
          fontStyle: 'Arial',
          sidePadding: 20
        }
      },
      tooltips: {
        callbacks: {
          label: function (tooltipItem, data) {
            var label = data['labels'][[tooltipItem['index']]];
            if (label) {
              var duration = data['datasets'][0]['data'][tooltipItem['index']];
              label += ': ';
              label += convertDuration(duration, 'HH:mm:ss');
            }
            return label;
          }
        }
      }
    };
  }

  function updateBarChart(self, data) {
    var weekStart = _.get(data, 'weekStart', 0);
    var totalTime = _.get(data, 'totalTime', []);
    var totalDuration = 0;

    self.barChartData.data.labels.length = 0;
    self.barChartData.data.labels.push(['Sun', moment(weekStart).add(0, 'days').format('DD/MM/YYYY')]);
    self.barChartData.data.labels.push(['Mon', moment(weekStart).add(1, 'days').format('DD/MM/YYYY')]);
    self.barChartData.data.labels.push(['Tue', moment(weekStart).add(2, 'days').format('DD/MM/YYYY')]);
    self.barChartData.data.labels.push(['Wed', moment(weekStart).add(3, 'days').format('DD/MM/YYYY')]);
    self.barChartData.data.labels.push(['Thu', moment(weekStart).add(4, 'days').format('DD/MM/YYYY')]);
    self.barChartData.data.labels.push(['Fri', moment(weekStart).add(5, 'days').format('DD/MM/YYYY')]);
    self.barChartData.data.labels.push(['Sat', moment(weekStart).add(6, 'days').format('DD/MM/YYYY')]);

    self.barChartData.data.datasets[0].data = totalTime;

    _.forEach(totalTime, function (time) {
      totalDuration = totalDuration + time;
    });

    self.totalTimeSpent = convertDuration(totalDuration, 'HH:mm:ss');

    self.barChart.update();
  }

  function updatePieChart(self, data) {
    var weekStart = _.get(data, 'weekStart', 0);
    var timeByProject = _.get(data, 'getTimeByProjects', []);
    var totalDuration = 0;

    self.pieChartData.data.labels.length = 0;
    self.pieChartData.data.datasets[0].data.length = 0;

    _.forEach(timeByProject, function (obj) {
      self.pieChartData.data.labels.push(obj.projectName);
      self.pieChartData.data.datasets[0].data.push(obj.duration);
      totalDuration = totalDuration + obj.duration;
    });

    self.pieChartData.centerText.display = true;
    self.pieChartData.centerText.text = convertDuration(totalDuration, 'HH:mm') + ' h';

    self.pieChart.update();
  }

  function updateSummaryTable(self, data) {
    self.summaryTable = _.get(data, 'getTimeByProjects', []);
    self.summaryTableSorted = _.cloneDeep(self.summaryTable);
  }

  function updatedDetailedTable(self, data) {
    self.detailedTable = _.get(data, 'getTimeByProjects', []);
    self.detailedTableSorted = _.cloneDeep(self.detailedTable);

    _.forEach(self.detailedTableSorted, function (item) {
      item.duration = item.to - item.from;
      item.time = convertDuration(item.from, 'HH:mm:ss DD/MM/YYYY') + ' - ' +
        convertDuration(item.to, 'HH:mm:ss DD/MM/YYYY');
    });
  }

  function getProjects(self, config) {
    var tmp;

    axios
      .get('/projects', config)
      .then(function (response) {
        var resData = _.get(response, 'data');

        self.createdProjects.length = 0;
        _.forEach(resData, function (p) {
          tmp = _.cloneDeep(p);
          tmp.selected = false;
          self.createdProjects.push(tmp);
        });
      })
      .catch(function (error) {

      });
  }

  function sortTable(self, sortKey) {
    if (self.sortBy === sortKey) {
      self.sortOrder = (self.sortOrder === 'asc' ? 'desc' : (self.sortOrder === 'desc' ? '' : 'asc'));
    } else {
      self.sortOrder = 'asc';
      self.sortBy = sortKey;
    }
  }

  function showPicker(self) {
    $(self.pickerSelector).datepicker('show');
  }

  function calculateWeekDate(self) {
    var dateFormat = 'DD/MM/YYYY';

    self.currentDate = self.currentDate || moment(new Date()).format(dateFormat);
    self.weekStart = moment(self.currentDate, dateFormat).day(0).valueOf();

    var firstDate = moment(self.currentDate, dateFormat).day(0).format(dateFormat);
    var lastDate = moment(self.currentDate, dateFormat).day(6).format(dateFormat);

    self.weekDate = firstDate + ' - ' + lastDate;

    self.$forceUpdate();
  }

  function addProject(self, proj) {
    proj.selected = !proj.selected;
    self.selectedProjects.length = 0;
    _.forEach(self.createdProjects, function (p) {
      if (p.selected) {
        self.selectedProjects.push(p.id);
      }
    });
  }

  function exportPdf(self, type) {
    var canvas = document.querySelector('#' + self.barChartId);
    //creates image
    var canvasImg = canvas.toDataURL();

    //creates PDF from img
    var doc = new jsPDF('landscape');
    doc.setFontSize(20);
    doc.text(10, 20, 'LogWork - ' + type + ' Reports');
    doc.setFontSize(18);
    doc.text(10, 40, 'Total: ' + self.totalTimeSpent);
    doc.addImage(canvasImg, 'PNG', 10, 60, 280, 110);
    doc.save(moment().format('DD-MM-YYYY') + '-logwork-' + type.toLowerCase() + '-reports.pdf');
  }

  function printChart(self, type) {
    var tWindow = window.open('', '', 'LogWork Print Chart');
    html2canvas($('#' + self.barChartId).get(0)).then(
      function (canvas) {
        var myImage = canvas.toDataURL('image/png');
        $(tWindow.document.body)
          .html('<html>' +
            '<head><title>LogWork Print Chart</title></head>' +
            '<body><h1>LogWork - ' + type + ' Reports</h1><br><br>' +
            '<h2>Total: ' + self.totalTimeSpent + '</h2><br><br>' +
            '<img id=\"LogWork - ' + type + ' Reports\" src=\"' + myImage + '\" style=\"width: 100%;\"/>' +
            '</body></html>')
          .ready(function () {
            tWindow.document.close();
            tWindow.focus();
            tWindow.print();
            tWindow.close();
          });
      });
  }

  Vue.component('lw-reports-summary', function (resolve, reject) {
    axios
      .get('./templates/reportsSummary.html')
      .then(function (template) {
        resolve({
          data: function () {
            return {
              barChartId: 'bar-chart',
              barChartData: {
                type: 'bar',
                data: {
                  labels: [],
                  datasets: [
                    {
                      data: [],
                      label: 'Total Time: ',
                      backgroundColor: getColorCode(),
                    }
                  ]
                },
                options: generateBartChartConfig()
              },
              pieChartId: 'pie-chart',
              pieChartData: {
                type: 'doughnut',
                data: {
                  labels: [],
                  datasets: [
                    {
                      data: [],
                      backgroundColor: getColorCode()
                    }
                  ]
                },
                options: generatePieChartConfig(),
                centerText: {
                  display: false
                }
              },
              totalTimeSpent: 0,
              summaryTable: [],
              sortBy: '',
              sortOrder: '',
              pickerSelector: '#weekPicker',
              weekDate: '',
              currentDate: 0,
              descFilter: '',
              weekStart: 0,
              createdProjects: [],
              selectedProjects: []
            }
          },
          methods: {
            getData: function () {
              var self = this;
              var accessToken = document.cookie.replace(/(?:(?:^|.*;\s*)accessToken\s*\=\s*([^;]*).*$)|^.*$/, "$1");
              var config = {
                headers: {
                  Authorization: 'Bearer ' + accessToken
                }
              };
              var request = {
                weekStart: self.weekStart,
                projects: self.selectedProjects,
                teams: [],
                searchTearm: self.descFilter
              };
              axios
                .post('/reports/sumary-project', request, config)
                .then(function (response) {
                  var resData = _.get(response, 'data');

                  if (!resData) {
                    return;
                  }

                  updateBarChart(self, resData);
                  updatePieChart(self, resData);
                  updateSummaryTable(self, resData);

                  self.getProject(config);

                  self.selectedProjects.length = 0;
                })
                .catch(function (error) {

                });
            },
            getProject: function (config) {
              var self = this;
              getProjects(self, config);
            },
            sortTable: function (sortKey) {
              var self = this;
              sortTable(self, sortKey);
            },
            showPicker: function () {
              var self = this;
              showPicker(self);
            },
            applyFilter: function () {
              var self = this;

              self.calculateWeekDate();
              self.getData();
            },
            calculateWeekDate: function () {
              var self = this;
              calculateWeekDate(self);
            },
            addProject: function (proj) {
              var self = this;
              addProject(self, proj);
            },
            exportPdf: function () {
              var self = this;
              exportPdf(self, 'Summary');
            },
            printChart: function () {
              var self = this;
              printChart(self, 'Summary');
            }
          },
          computed: {
            summaryTableSorted: function () {
              var self = this;

              if (!self.sortOrder) {
                self.sortBy = '';
                return self.summaryTable;
              }
              return _.orderBy(self.summaryTable, self.sortBy, self.sortOrder);
            }
          },
          mounted: function () {
            var self = this;

            var barChartCtx = document.getElementById(self.barChartId);
            self.barChart = new Chart(barChartCtx, self.barChartData);

            var pieChartCtx = document.getElementById(self.pieChartId).getContext('2d');
            self.pieChart = new Chart(pieChartCtx, self.pieChartData);

            var weekPicker = $(self.pickerSelector);

            weekPicker.datepicker({
              format: 'dd/mm/yyyy',
              autoclose: true,
              orientation: 'right',
              todayBtn: 'linked',
              todayHighlight: true
            });

            weekPicker.on('hide', function () {
              self.currentDate = weekPicker.val();
              self.calculateWeekDate();
              self.getData();
            });

            self.calculateWeekDate();
            self.getData();
          },
          filters: {
            convertDuration: function (value) {
              return convertDuration(value, 'HH:mm:ss');
            }
          },
          template: _.get(template, 'data')
        });
      })
      .catch(function (error) {
        reject('ERROR: ', error);
      });
  });

  Vue.component('lw-reports-detailed', function (resolve, reject) {
    axios
      .get('./templates/reportsDetailed.html')
      .then(function (template) {
        resolve({
          data: function () {
            return {
              barChartId: 'bar-chart',
              barChartData: {
                type: 'bar',
                data: {
                  labels: [],
                  datasets: [
                    {
                      data: [],
                      label: 'Total Time: ',
                      backgroundColor: getColorCode(),
                    }
                  ]
                },
                options: generateBartChartConfig()
              },
              totalTimeSpent: 0,
              detailedTable: [],
              sortBy: '',
              sortOrder: '',
              pickerSelector: '#weekPicker',
              weekDate: '',
              currentDate: 0,
              descFilter: '',
              weekStart: 0,
              createdProjects: [],
              selectedProjects: []
            }
          },
          methods: {
            getData: function () {
              var self = this;
              var accessToken = document.cookie.replace(/(?:(?:^|.*;\s*)accessToken\s*\=\s*([^;]*).*$)|^.*$/, "$1");
              var config = {
                headers: {
                  Authorization: 'Bearer ' + accessToken
                }
              };
              var request = {
                weekStart: self.weekStart,
                projects: self.selectedProjects,
                teams: [],
                searchTearm: self.descFilter
              };
              axios
                .post('/reports/sumary-task', request, config)
                .then(function (response) {
                  var resData = _.get(response, 'data');

                  if (!resData) {
                    return;
                  }

                  updateBarChart(self, resData);
                  updatedDetailedTable(self, resData);

                  self.getProject(config);

                  self.selectedProjects.length = 0;
                })
                .catch(function (error) {

                });
            },
            getProject: function (config) {
              var self = this;
              getProjects(self, config);
            },
            sortTable: function (sortKey) {
              var self = this;
              sortTable(self, sortKey);
            },
            showPicker: function () {
              var self = this;
              showPicker(self);
            },
            applyFilter: function () {
              var self = this;

              self.calculateWeekDate();
              self.getData();
            },
            calculateWeekDate: function () {
              var self = this;
              calculateWeekDate(self);
            },
            addProject: function (proj) {
              var self = this;
              addProject(self, proj);
            },
            exportPdf: function () {
              var self = this;
              exportPdf(self, 'Detailed');
            },
            printChart: function () {
              var self = this;
              printChart(self, 'Detailed');
            }
          },
          computed: {
            detailedTableSorted: function () {
              var self = this;

              if (!self.sortOrder) {
                self.sortBy = '';
                return self.detailedTable;
              }
              return _.orderBy(self.detailedTable, self.sortBy, self.sortOrder);
            }
          },
          mounted: function () {
            var self = this;

            var barChartCtx = document.getElementById(self.barChartId);
            self.barChart = new Chart(barChartCtx, self.barChartData);

            var weekPicker = $(self.pickerSelector);

            weekPicker.datepicker({
              format: 'dd/mm/yyyy',
              autoclose: true,
              orientation: 'right',
              todayBtn: 'linked',
              todayHighlight: true
            });

            weekPicker.on('hide', function () {
              self.currentDate = weekPicker.val();
              self.calculateWeekDate();
              self.getData();
            });

            self.calculateWeekDate();
            self.getData();
          },
          filters: {
            convertDuration: function (value) {
              return convertDuration(value, 'HH:mm:ss');
            }
          },
          template: _.get(template, 'data')
        });
      })
      .catch(function (error) {
        reject('ERROR: ', error);
      });
  });

})(window.axios, window._, window.moment, window.Chart, window.jQuery, window.jsPDF, window.html2canvas);